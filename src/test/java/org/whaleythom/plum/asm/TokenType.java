package org.whaleythom.plum.asm;

public enum TokenType {
    LINE_BREAK("a line break", "line breaks"),
    LABEL("a label", "labels"),
    LABEL_REFERENCE("a label reference", "label references"),
    REGISTER("a register", "registers"),
    IDENTIFIER("an identifier", "identifiers"),
    INTEGER("an integer", "integers"),
    HEXADECIMAL("a hexadecimal number", "hexadecimal numbers"),
    INSTRUCTION("an instruction", "instructions"),
    KEYWORD("a keyword", "keywords");

    private final String humanReadableSingular;
    private final String humanReadablePlural;

    TokenType(String humanReadableSingular, String humanReadablePlural) {
        this.humanReadableSingular = humanReadableSingular;
        this.humanReadablePlural = humanReadablePlural;
    }

    public String humanReadable() {
        return humanReadableSingular;
    }

    public String humanReadablePlural() {
        return humanReadablePlural;
    }

    public boolean isNumber() {
        return this == INTEGER || this == HEXADECIMAL;
    }
}
