package org.whaleythom.plum.asm;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class Compiler {
    public static final byte OP_LOAD_A = 1;
    public static final byte OP_LOAD_B = 2;
    public static final byte OP_LOAD_C = 3;
    public static final byte OP_LOAD_STATUS = 4;
    public static final byte OP_ADD = 5;
    public static final byte OP_SUB = 6;
    public static final byte OP_JUMP = 7;
    public static final byte OP_CALL = 8;
    public static final byte OP_RETURN = 9;
    public static final byte OP_PUSH = 10;
    public static final byte OP_POP = 11;
    public static final byte OP_WRITE_A = 12;
    public static final byte OP_WRITE_B = 13;
    public static final byte OP_WRITE_C = 14;
    public static final byte OP_WRITE_STATUS = 15;
    public static final byte OP_READ_A = 16;
    public static final byte OP_READ_B = 17;
    public static final byte OP_READ_C = 18;
    public static final byte OP_READ_STATUS = 19;
    public static final byte OP_COPY_A_B = 20;
    public static final byte OP_COPY_A_C = 21;
    public static final byte OP_COPY_A_STATUS = 22;
    public static final byte OP_COPY_B_A = 23;
    public static final byte OP_COPY_C_A = 24;
    public static final byte OP_COPY_STATUS_A = 25;
    public static final byte OP_CMP = 26;
    public static final byte OP_INC = 27;
    public static final byte OP_AND = 28;
    public static final byte OP_OR = 29;
    public static final byte OP_XOR = 30;
    public static final byte OP_NOT = 31;
    public static final byte OP_RELATIVE_JUMP = 32;
    public static final byte OP_RELATIVE_CALL = 33;

    public static final Set<String> INSTRUCTIONS = Set.of(
            "add",
            "and",
            "call",
            "cmp",
            "inc",
            "jump",
            "mov",
            "not",
            "or",
            "pop",
            "push",
            "return",
            "sub",
            "xor"
    );
    public static final Set<String> KEYWORDS = Set.of(
            "const"
    );

    public int getNumRequiredBytes(int value) {
        if (value == 0) { return 1; }
        return (int) (Math.ceil(Math.log(value + 1) / Math.log(2) / 8f));
    }

    public int levenshteinDistance(String lhs, String rhs) {
        // https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Java
        int len0 = lhs.length() + 1;
        int len1 = rhs.length() + 1;
        int[] cost = new int[len0];
        int[] newcost = new int[len0];
        for (int i = 0; i < len0; i++) cost[i] = i;
        for (int j = 1; j < len1; j++) {
            newcost[0] = j;
            for(int i = 1; i < len0; i++) {
                int match = (lhs.charAt(i - 1) == rhs.charAt(j - 1)) ? 0 : 1;
                int cost_replace = cost[i - 1] + match;
                int cost_insert  = cost[i] + 1;
                int cost_delete  = newcost[i - 1] + 1;
                newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
            }
            int[] swap = cost; cost = newcost; newcost = swap;
        }
        return cost[len0 - 1];
    }

    public Optional<String> closest(String string, Collection<String> strings) {
        return strings.stream().filter(s -> levenshteinDistance(string, s) < 3).min((a, b) -> levenshteinDistance(string, a) - levenshteinDistance(string, b));
    }

    public List<Token> lex(String fileName) throws IOException {
        List<String> lines = Files.readAllLines(Path.of(fileName));
        List<Token> out = new ArrayList<>();
        for (int lineNum = 0; lineNum < lines.size(); lineNum ++) {
            String line = lines.get(lineNum);
            int col = 0;
            int startingPointer = 0;
            while (col < line.length()) {
                char c = line.charAt(col);
                if (Character.isWhitespace(c)) {
                    col ++;
                }
                else if (Character.isDigit(c)) {
                    startingPointer = col;
                    // Hex
                    if (c == '0' && col < line.length() - 1 && line.charAt(col + 1) == 'x') {
                        col += 2;
                        while (col < line.length() && !Character.isWhitespace(c = line.charAt(col))) {
                            if (Character.isDigit(c) || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F')) {
                                col ++;
                            } else {
                                String lexeme = line.substring(startingPointer, col + 1);
                                Error.of(Token.of(lexeme, TokenType.HEXADECIMAL,
                                                new Location(fileName, line, lineNum + 1, startingPointer + 1)),
                                        "Invalid hex expression. Only expects 0-9, a-f.", "Got `" + c + "`").show();
                            }
                        }
                        int length = col - startingPointer;
                        String lexeme = line.substring(startingPointer, col);
                        Token token = Token.of(lexeme, TokenType.HEXADECIMAL,
                                new Location(fileName, line, lineNum + 1, startingPointer + 1));
                        if (length == 2) {
                            Error.of(token, "Invalid hex expression. Expects trailing digits after 0x").show();
                        }
                        out.add(token);
                    } else {
                       // Integer
                       while (col < line.length() && !Character.isWhitespace(c = line.charAt(col))) {
                           if (!Character.isDigit(c)) {
                               String lexeme = line.substring(startingPointer, col + 1);
                               Error.of(Token.of(lexeme, TokenType.INTEGER,
                                               new Location(fileName, line, lineNum + 1, startingPointer + 1)),
                                       "Invalid int expression. Only expects 0-9", "Found `" + c + "`").show();
                           }
                           col ++;
                       }
                       String lexeme = line.substring(startingPointer, col);
                       Token token = Token.of(lexeme, TokenType.INTEGER, new Location(fileName, line, lineNum + 1, startingPointer + 1));
                       out.add(token);
                    }
                }
                else if (c == '-') {
                    Token token = Token.of("-", TokenType.IDENTIFIER, new Location(fileName, line, lineNum + 1, col + 1));
                    Error.of(token, "Negative numbers are not supported yet", "Delete this?").show();
                }
                else if (c == ';') {
                    break;
                }
                else {
                    startingPointer = col;
                    while (col < line.length() && !Character.isWhitespace(c = line.charAt(col))) {
                        col ++;
                    }
                    int length = col - startingPointer;
                    String lexeme = line.substring(startingPointer, col);
                    TokenType type = TokenType.IDENTIFIER;
                    if (INSTRUCTIONS.contains(lexeme)) {
                        type = TokenType.INSTRUCTION;
                    } else if (KEYWORDS.contains(lexeme)) {
                        type = TokenType.KEYWORD;
                    } else if (lexeme.endsWith(":")) {
                        type = TokenType.LABEL;
                    } else if (length == 2 && (lexeme.equals("ra") || lexeme.equals("rb") || lexeme.equals("rc") || lexeme.equals("rs"))) {
                        type = TokenType.REGISTER;
                    }
                    Token token = Token.of(lexeme, type, new Location(fileName, line, lineNum + 1, startingPointer + 1));
                    if (token.type() == TokenType.LABEL) {
                        String labelName = token.lexeme().substring(0, token.lexeme().length() - 1);
                        if (INSTRUCTIONS.contains(labelName)) {
                            Error.of(token, "Cannot have a label that is the same name as an instruction").show();
                        }
                        if (KEYWORDS.contains(labelName)) {
                            Error.of(token, "Cannot have a label that is the same name as a keyword").show();
                        }
                    }
                    out.add(token);
                }
            }
            if (!out.isEmpty() && out.get(out.size() - 1).type() != TokenType.LINE_BREAK) {
                Token token = Token.of("", TokenType.LINE_BREAK, new Location(fileName, line, lineNum + 1, col + 1));
                out.add(token);
            }
        }
        return out;
    }

    public Set<String> getLabels(List<Token> tokens) {
        Set<String> out = new HashSet<>();
        for (Token token : tokens) {
            if (token.type() != TokenType.LABEL) {
                continue;
            }
            String labelName = token.lexeme().substring(0, token.lexeme().length() - 1);
            if (out.contains(labelName)) {
                Error.of(token, "Duplicate label definition")
                        .addError(Error.of(token, "The first definition is here")).show();
            }
            out.add(labelName);
        }
        return out;
    }

    public List<Token> updateLabels(List<Token> tokens, Set<String> labels) {
        List<Token> out = new ArrayList<>();
        for (Token token : tokens) {
            if (token.type() == TokenType.IDENTIFIER && labels.contains(token.lexeme())) {
                out.add(Token.of(token.lexeme(), TokenType.LABEL_REFERENCE, token.location()));
            } else {
                out.add(token);
            }
        }
        return out;
    }

    public Token peek(ListIterator<Token> iterator) {
        Token object = iterator.next();
        iterator.previous();
        return object;
    }

    public Integer parseInteger(Token token, int maxBytes) {
        if (token.type() != TokenType.INTEGER) {
            Error.of(token, "Expects an integer")
                    .addMessage("Found " + token.type().humanReadable()).show();
        }
        int value = Integer.parseInt(token.lexeme());
        int numRequiredBytes = getNumRequiredBytes(value);
        if (numRequiredBytes > maxBytes) {
            Error.of(token, "Integer cannot fit into 1 byte",
                    "Takes up " + numRequiredBytes + " bytes").show();
        }
        return value;
    }

    public int parseHex(Token token, int maxBytes) {
        if (token.type() != TokenType.HEXADECIMAL) {
            Error.of(token, "Expects a hexadecimal number")
                    .addMessage("Found " + token.type().humanReadable()).show();
        }
        int value = Integer.parseInt(token.lexeme().substring(2), 16);
        int numRequiredBytes = getNumRequiredBytes(value);
        if (numRequiredBytes > maxBytes) {
            Error.of(token, "Hexadecimal value cannot fit into " + maxBytes + " byte(s)",
                    "Takes up " + numRequiredBytes + " bytes").show();
        }
        return value;
    }

    public int parseNumber(Token token, int maxBytes) {
        if (token.type() == TokenType.INTEGER) {
            return parseInteger(token, maxBytes);
        } else if (token.type() == TokenType.HEXADECIMAL) {
            return parseHex(token, maxBytes);
        }
        Error.of(token, "Expects a number")
                .addMessage("Found " + token.type().humanReadable()).show();
        return -1;
    }

    public Map.Entry<String, Token> parseConst(Token token, ListIterator<Token> iterator, Map<String, Token> constantExpressions) {
        if (!iterator.hasNext() || peek(iterator).type() == TokenType.LINE_BREAK) {
            Error.of(token, "Expects a name and expression after keyword `const`")
                    .addMessage("Try `const a 10`").show();
        }
        Token name = iterator.next();
        if (name.type() != TokenType.IDENTIFIER) {
            Error.of(name, "Invalid constant name")
                    .addMessage("This is " + name.type().humanReadable()).show();
        }
        if (!iterator.hasNext() || peek(iterator).type() == TokenType.LINE_BREAK) {
            Error.of(token, "Expects an expression after constant declaration")
                    .addMessage("Try `const " + name.lexeme() + " 10`").show();
        }
        // TODO: Make constant expression evaluate arithmetic
        // TODO: Make constant expression evaluate other constant expressions
        Token value = iterator.next();
        if (value.type() == TokenType.IDENTIFIER) {
            if (constantExpressions.containsKey(value.lexeme())) {
                Error.of(value, "Invalid constant expression")
                        .addMessage("Recursive expansion not allowed (yet)").show();
            } else {
                Optional<String> misspelling = closest(value.lexeme(), constantExpressions.keySet());
                Error error = Error.of(value, "Invalid constant expression");
                if (misspelling.isPresent()) {
                    error = error.addMessage("Perhaps you meant " + misspelling.get() + "?");
                } else {
                    error = error.addMessage("Perhaps you misspelled a constant?");
                }
                error.show();
            }
        } else if (!value.type().isNumber()){
            Error.of(value, "Invalid constant expression")
                    .addMessage("Not a number").show();
        }
        if (iterator.hasNext()) {
            Token lineBreak = iterator.next();
            if (lineBreak.type() != TokenType.LINE_BREAK) {
                Error.of(lineBreak, "Expects a line break in const expression")
                        .addMessage("Found " + lineBreak.type().humanReadable() + " instead").show();
            }
        };
        if (constantExpressions.containsKey(name.lexeme())) {
            Token previouslyDefinedToken = constantExpressions.get(name.lexeme());
            Error.of(value, "Redefinition of constant expression")
                    .addError(Error.of(previouslyDefinedToken, "Original definition is here")).show();
        }
        return Map.entry(name.lexeme(), value);
    }

    public String parseLabel(Token token, ListIterator<Token> iterator) {
        if (token.type() != TokenType.LABEL) {
            Error.of(token, "Expects " + TokenType.LABEL.humanReadable() + " to encapsulate a block.",
                            "Found " + token.type().humanReadable())
                    .addMessage("Perhaps add `label:` before?").show();
        }
        String label = token.lexeme().substring(0, token.lexeme().length() - 1);
        if (iterator.hasNext()) {
            Token lineBreak = iterator.next();
            if (lineBreak.type() != TokenType.LINE_BREAK) {
                Error.of(lineBreak, "Expects a line break after label definition",
                        "Found " + lineBreak.type().humanReadable()).show();
            }
        }
        return label;
    }

    public List<CompilerToken> parseInstruction(Token instruction, ListIterator<Token> iterator, Set<String> labels,
                                                           Map<String, Token> constantExpressions, boolean canImplicitlyUseTempRegister) {
        List<CompilerToken> out = new ArrayList<>();
        if (instruction.type() != TokenType.INSTRUCTION) {
            if (instruction.type() == TokenType.IDENTIFIER) {
                Optional<String> misspelling = closest(instruction.lexeme(), INSTRUCTIONS);
                Error error = Error.of(instruction, "Invalid instruction");
                if (misspelling.isPresent()) {
                    error = error.addMessage("Perhaps you meant " + misspelling.get() + "?");
                } else {
                    error = error.addMessage("Perhaps you misspelled an instruction?");
                }
                error.show();
            } else if (instruction.type() == TokenType.KEYWORD) {
                if (instruction.lexeme().equals("const")) {
                    Error.of(instruction, "Invalid instruction")
                            .addMessage("Constant expression should be defined at the top of a block (just below a label)").show();
                }
            }
            Error.of(instruction, "Expects an instruction",
                    "Found " + instruction.type().humanReadable()).show();
        }

        switch (instruction.lexeme()) {
            case "mov" -> {
                if (!iterator.hasNext() || peek(iterator).type() == TokenType.LINE_BREAK) {
                    Error.of(instruction, "`mov` instruction expects 2 arguments", "Found no arguments")
                            .addMessage("These are the value to move from, and the value to move to").show();
                }
                Token from = iterator.next();
                if (!iterator.hasNext() || peek(iterator).type() == TokenType.LINE_BREAK) {
                    Error.of(instruction, "`mov` instruction expects 2 arguments", "Found 1 argument")
                            .addMessage("These are the value to move from, and the value to move to").show();
                }
                Token to = iterator.next();
                if (to.type() == TokenType.LINE_BREAK) {
                    Error.of(instruction, "`mov` instruction expects 2 arguments", "Found 1 argument")
                            .addMessage("These are the value to move from, and the value to move to").show();
                }
                if (from.type() == TokenType.REGISTER && to.type() == TokenType.REGISTER) {
                    // Check for invalid mov arguments
                    if (from.lexeme().equals("ra") && to.lexeme().equals("ra")) {
                        if (canImplicitlyUseTempRegister) {
                            // TODO: Implicit use of temp register
                            Error.of("Compilation with the use of a temporary register is not currently implemented").show();
                        }
                        Error.of(instruction, "Cannot move ra to ra.")
                                .addMessage("Cannot perform this without implicitly using another register")
                                // TODO: Implicit use of temp register
                                .addMessage("Have you considered enabling the implicit use of a temporary register?").show();
                    }
                    if (!from.lexeme().equals("ra") && !to.lexeme().equals("ra")) {
                        if (canImplicitlyUseTempRegister) {
                            // TODO: Implicit use of temp register
                            Error.of("Compilation with the use of a temporary register is not currently implemented").show();
                        }
                        Error.of(instruction, "Cannot move " + from.lexeme() + " to " + to.lexeme() + ".")
                                .addMessage("Cannot perform this without implicitly using another register")
                                // TODO: Implicit use of temp register
                                .addMessage("Have you considered enabling the implicit use of a temporary register?").show();
                    }

                    if (from.lexeme().equals("ra")) {
                        switch (to.lexeme()) {
                            case "rb" -> out.add(CompilerToken.of(instruction, "COPY_A_B", OP_COPY_A_B));
                            case "rc" -> out.add(CompilerToken.of(instruction, "COPY_A_C", OP_COPY_A_C));
                            case "rs" -> out.add(CompilerToken.of(instruction, "COPY_A_STATUS", OP_COPY_A_STATUS));
                            default -> {
                                Error.of(to, "Invalid register")
                                        .addMessage("This indicates a bug in the lexer").show();
                            }
                        }
                    } else {
                        switch (from.lexeme()) {
                            case "rb" -> out.add(CompilerToken.of(instruction, "COPY_B_A", OP_COPY_B_A));
                            case "rc" -> out.add(CompilerToken.of(instruction, "COPY_C_A", OP_COPY_C_A));
                            case "rs" -> out.add(CompilerToken.of(instruction, "COPY_STATUS_A", OP_COPY_STATUS_A));
                            default -> {
                                Error.of(from, "Invalid register")
                                        .addMessage("This indicates a bug in the lexer").show();
                            }
                        }
                    }
                } else if (from.type() == TokenType.REGISTER || to.type() == TokenType.REGISTER) {
                    Token register = from.type() == TokenType.REGISTER ? from : to;
                    Token other = from.type() != TokenType.REGISTER ? from : to;
                    switch (register.lexeme()) {
                        case "ra" -> out.add(CompilerToken.of(instruction, "LD_A_IMM", OP_LOAD_A));
                        case "rb" -> out.add(CompilerToken.of(instruction, "LD_B_IMM", OP_LOAD_B));
                        case "rc" -> out.add(CompilerToken.of(instruction, "LD_C_IMM", OP_LOAD_C));
                        case "rs" -> out.add(CompilerToken.of(instruction, "LD_STATUS_IMM", OP_LOAD_STATUS));
                        default -> {
                            Error.of(from, "Invalid register")
                                    .addMessage("This indicates a bug in the lexer").show();
                        }
                    }
                    if (other.type() == TokenType.IDENTIFIER && constantExpressions.containsKey(other.lexeme())) {
                        Token evaluated = constantExpressions.get(other.lexeme());
                        if (evaluated.type().isNumber()) {
                            int value = parseNumber(evaluated, 1);
                            out.add(CompilerToken.arg(other, (byte) value));
                        } else {
                            Error.of(other, "Invalid constant expression expansion")
                                    .addError(Error.of(evaluated, "Constant expression lives here")).show();
                        }
                    } else if (other.type().isNumber()) {
                        int value = parseNumber(other, 1);
                        out.add(CompilerToken.arg(other, (byte) value));
                    } else {
                        Error.of(other, "Invalid argument",
                                        "Unable to move " + to.type().humanReadable() + " to a register")
                                .addMessage("Expects a number").show();
                    }
                } else {
                    Error.of(instruction, "Expects at least one argument in `mov` to be a register.")
                            .addMessage("Cannot move " + from.type().humanReadable() + " to " + to.type().humanReadable())
                            .show();
                }
            }
            case "add" -> {
                if (iterator.hasNext() && peek(iterator).type() != TokenType.LINE_BREAK) {
                    Error.of(instruction, "`add` instruction expects no arguments")
                            .addMessage("The add instruction adds rb and rc to ra").show();
                }
                out.add(CompilerToken.of(instruction, "ADD", OP_ADD));
            }
            case "sub" -> {
                if (iterator.hasNext() && peek(iterator).type() != TokenType.LINE_BREAK) {
                    Error.of(instruction, "`sub` instruction expects no arguments")
                            .addMessage("The sub instruction subtracts rb and rc to ra").show();
                }
                out.add(CompilerToken.of(instruction, "SUB", OP_SUB));
            }
            case "cmp" -> {
                if (iterator.hasNext() && peek(iterator).type() != TokenType.LINE_BREAK) {
                    Error.of(instruction, "`cmp` instruction expects no arguments")
                            .addMessage("The cmp instruction compares rb to rc").show();
                }
                out.add(CompilerToken.of(instruction, "CMP", OP_CMP));
            }
            case "inc" -> {
                if (iterator.hasNext() && peek(iterator).type() != TokenType.LINE_BREAK) {
                    Error.of(instruction, "`inc` instruction expects no arguments")
                            .addMessage("The inc instruction increments ra").show();
                }
                out.add(CompilerToken.of(instruction, "INC", OP_INC));
            }
            case "and" -> {
                if (iterator.hasNext() && peek(iterator).type() != TokenType.LINE_BREAK) {
                    Error.of(instruction, "`and` instruction expects no arguments")
                            .addMessage("The and instruction performs bitwise and on rb and rc to ra").show();
                }
                out.add(CompilerToken.of(instruction, "AND", OP_AND));
            }
            case "or" -> {
                if (iterator.hasNext() && peek(iterator).type() != TokenType.LINE_BREAK) {
                    Error.of(instruction, "`and` instruction expects no arguments")
                            .addMessage("The and instruction performs bitwise or on rb and rc to ra").show();
                }
                out.add(CompilerToken.of(instruction, "OR", OP_OR));
            }
            case "xor" -> {
                if (iterator.hasNext() && peek(iterator).type() != TokenType.LINE_BREAK) {
                    Error.of(instruction, "`xor` instruction expects no arguments")
                            .addMessage("The xor instruction performs bitwise xor on rb and rc to ra").show();
                }
                out.add(CompilerToken.of(instruction, "XOR", OP_XOR));
            }
            case "not" -> {
                if (iterator.hasNext() && peek(iterator).type() != TokenType.LINE_BREAK) {
                    Error.of(instruction, "`not` instruction expects no arguments")
                            .addMessage("The not instruction performs bitwise not ra").show();
                }
                out.add(CompilerToken.of(instruction, "NOT", OP_NOT));
            }
            case "push" -> {
                if (iterator.hasNext() && peek(iterator).type() != TokenType.LINE_BREAK) {
                    Error.of(instruction, "`push` instruction expects no arguments")
                            .addMessage("The push instruction pushes ra to the top of the stack").show();
                }
                out.add(CompilerToken.of(instruction, "PUSH", OP_PUSH));
            }
            case "pop" -> {
                if (iterator.hasNext() && peek(iterator).type() != TokenType.LINE_BREAK) {
                    Error.of(instruction, "`pop` instruction expects no arguments")
                            .addMessage("The pop instruction pops the top value from the stack into ra").show();
                }
                out.add(CompilerToken.of(instruction, "POP", OP_POP));
            }
            case "return" -> {
                if (iterator.hasNext() && peek(iterator).type() != TokenType.LINE_BREAK) {
                    Error.of(instruction, "`return` instruction expects no arguments")
                            .addMessage("The return instruction returns to the last place `call` was called from").show();
                }
                out.add(CompilerToken.of(instruction, "RETURN", OP_RETURN));
            }
            case "call" -> {
                if (!iterator.hasNext() || peek(iterator).type() == TokenType.LINE_BREAK) {
                    Error.of(instruction, "`call` instruction expects 1 argument",
                                    "Found no argument")
                            .addMessage("This argument is the jump destination")
                            .addMessage("   Could be a number (address) or a label").show();
                }
                Token address = iterator.next();
                if (address.type() == TokenType.LABEL_REFERENCE) {
                    out.add(CompilerToken.of(instruction, "RELATIVE_CALL", OP_RELATIVE_CALL));
                    out.add(CompilerToken.reference(address, address.lexeme()));
                } else if (address.type().isNumber()) {
                    int value = parseNumber(address, 2);
                    out.add(CompilerToken.of(instruction, "CALL", OP_CALL));
                    byte msb = (byte) ((value & 0xff00) >> 8);
                    byte lsb = (byte) (value & 0xff);
                    out.add(CompilerToken.arg(address, lsb));
                    out.add(CompilerToken.arg(address, msb));
                } else if (address.type() == TokenType.IDENTIFIER) {
                    if (constantExpressions.containsKey(address.lexeme())) {
                        Token evaluated = constantExpressions.get(address.lexeme());
                        if (evaluated.type().isNumber()) {
                            int value = parseNumber(evaluated, 2);
                            out.add(CompilerToken.of(instruction, "CALL", OP_CALL));
                            byte msb = (byte) ((value & 0xff00) >> 8);
                            byte lsb = (byte) (value & 0xff);
                            out.add(CompilerToken.arg(address, lsb));
                            out.add(CompilerToken.arg(address, msb));
                        } else {
                            Error.of(address, "Invalid constant expansion")
                                    .addError(Error.of(evaluated, "This is not a number")
                                            .addMessage("Found " + evaluated.type().humanReadable())).show();
                        }
                    } else {
                        Optional<String> misspelling = closest(address.lexeme(), labels);
                        Error error = Error.of(address, "Invalid call argument");
                        if (misspelling.isPresent()) {
                            error = error.addMessage("Perhaps you meant " + misspelling.get() + "?");
                        } else {
                            error = error.addMessage("Perhaps you misspelled a label name");
                        }
                        error.show();
                    }
                } else {
                    Error.of(address, "Invalid call argument",
                                    "This is " + address.type().humanReadable())
                            .addMessage("Expects a number or label reference").show();
                }
            }
            case "jump" -> {
                if (!iterator.hasNext() || peek(iterator).type() == TokenType.LINE_BREAK) {
                    Error.of(instruction, "`jump` instruction expects 1 argument",
                                    "Found no argument")
                            .addMessage("This argument is the jump destination")
                            .addMessage("   Could be a number (address) or a label").show();
                }
                Token address = iterator.next();
                if (address.type() == TokenType.LABEL_REFERENCE) {
                    out.add(CompilerToken.of(instruction, "RELATIVE_JUMP", OP_RELATIVE_JUMP));
                    out.add(CompilerToken.reference(address, address.lexeme()));
                } else if (address.type().isNumber()) {
                    int value = parseNumber(address, 2);
                    out.add(CompilerToken.of(instruction, "JUMP_IMM", OP_JUMP));
                    byte msb = (byte) ((value & 0xff00) >> 8);
                    byte lsb = (byte) (value & 0xff);
                    out.add(CompilerToken.arg(address, lsb));
                    out.add(CompilerToken.arg(address, msb));
                } else if (address.type() == TokenType.IDENTIFIER) {
                    if (constantExpressions.containsKey(address.lexeme())) {
                        Token evaluated = constantExpressions.get(address.lexeme());
                        if (evaluated.type().isNumber()) {
                            int value = parseNumber(evaluated, 2);
                            out.add(CompilerToken.of(instruction, "JUMP_IMM", OP_JUMP));
                            byte msb = (byte) ((value & 0xff00) >> 8);
                            byte lsb = (byte) (value & 0xff);
                            out.add(CompilerToken.arg(address, lsb));
                            out.add(CompilerToken.arg(address, msb));
                        } else {
                            Error.of(address, "Invalid constant expression expansion")
                                    .addError(Error.of(evaluated, "Expanded from here", "Found " + evaluated.type().humanReadable()))
                                    .addMessage("Expects number").show();
                        }
                    } else {
                        Optional<String> misspelling = closest(address.lexeme(), labels);
                        Error error = Error.of(address, "Invalid call argument");
                        if (misspelling.isPresent()) {
                            error = error.addMessage("Perhaps you meant " + misspelling.get() + "?");
                        } else {
                            error = error.addMessage("Perhaps you misspelled a label name");
                        }
                        error.show();
                    }
                } else {
                    Error.of(address, "Invalid jump argument",
                                    "This is " + address.type().humanReadable())
                            .addMessage("Expects a number or label reference").show();
                }
            }
            default -> {
                Optional<String> misspelling = closest(instruction.lexeme(), INSTRUCTIONS);
                Error error = Error.of(instruction, "Invalid call argument");
                if (misspelling.isPresent()) {
                    error = error.addMessage("Perhaps you meant " + misspelling.get() + "?");
                } else {
                    error = error.addMessage("Perhaps you misspelled a label name");
                }
                error.show();
            }
        }
        if (iterator.hasNext()) {
            Token newLine = iterator.next();
            if (newLine.type() != TokenType.LINE_BREAK) {
                Error.of(newLine, "Expected a line break")
                        .addMessage("Found " + newLine.type().humanReadable())
                        .addMessage("This is most likely a bug in the lexer").show();
            }
        }
        return out;
    }

    public List<CompilerToken> parseBlock(ListIterator<Token> iterator, Set<String> labels, Map<String, Token> constantExpressions, boolean canImplicitlyUseTempRegister) {
        List<CompilerToken> out = new ArrayList<>();
        while (iterator.hasNext() && peek(iterator).type() != TokenType.LABEL) {
            Token instruction = iterator.next();
            List<CompilerToken> tokens = parseInstruction(instruction, iterator, labels, constantExpressions, canImplicitlyUseTempRegister);
            out.addAll(tokens);
        }
        return out;
    }

    public List<CompilerToken> compile(List<Token> tokens, Set<String> labels, boolean canImplicitlyUseTempRegister) {
        ListIterator<Token> iterator = tokens.listIterator();
        List<CompilerToken> out = new ArrayList<>();
        Map<String, Token> constantExpressions = new HashMap<>();
        while (iterator.hasNext() && peek(iterator).type() == TokenType.KEYWORD) {
            Token keyword = iterator.next();
            if (keyword.lexeme().equals("const")) {
                Map.Entry<String, Token> constExpr = parseConst(keyword, iterator, constantExpressions);
                constantExpressions.put(constExpr.getKey(), constExpr.getValue());
            } else {
                Error.of(keyword, "Unexpected keyword")
                        .addMessage("Perhaps this should go straight into a label?").show();
            }
        }
        while (iterator.hasNext()) {
            Token token = iterator.next();
            String label = parseLabel(token, iterator);
            out.add(CompilerToken.label(token, label));
            Map<String, Token> blockExpressions = new HashMap<>(constantExpressions);
            while (iterator.hasNext() && peek(iterator).type() == TokenType.KEYWORD) {
                Token keyword = iterator.next();
                if (keyword.lexeme().equals("const")) {
                    Map.Entry<String, Token> constExpr = parseConst(keyword, iterator, blockExpressions);
                    blockExpressions.put(constExpr.getKey(), constExpr.getValue());
                } else {
                    Error.of(keyword, "Unexpected keyword")
                            .addMessage("Perhaps this should go straight into a label?").show();
                }
            }

            List<CompilerToken> toAdd = parseBlock(iterator, labels, blockExpressions, canImplicitlyUseTempRegister);
            out.addAll(toAdd);
        }
        return out;
    }

    public List<CompilerToken> jumpLabelReferences(List<CompilerToken> tokens) {
        Map<String, Integer> labelIndices = new HashMap<>();
        int index = 0;
        for (CompilerToken token : tokens) {
            if (token.isLabel()) {
                labelIndices.put(token.instruction(), index);
                continue;
            }
            index ++;
        }

        index = 0;
        List<CompilerToken> out = new ArrayList<>();
        for (CompilerToken token : tokens) {
            if (token.isLabel()) continue;
            if (token.isReference()) {
                if (labelIndices.containsKey(token.instruction())) {
                    int labelIndex = labelIndices.get(token.instruction());
                    int indexDifference = labelIndex - index;
                    out.add(CompilerToken.arg(token.token(), (byte) indexDifference));
                    index ++;
                    continue;
                }
            }
            out.add(token);
            index++;
        }
        return out;
    }
}
