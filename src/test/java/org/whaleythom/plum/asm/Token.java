package org.whaleythom.plum.asm;

import java.util.ArrayList;
import java.util.List;

public class Token {
    private final String lexeme;
    private final TokenType type;
    private final Location location;
    private final List<Token> expanded = new ArrayList<>();

    private Token(String lexeme, TokenType type, Location location) {
        this.lexeme = lexeme;
        this.type = type;
        this.location = location;
    }

    public static Token of(String lexeme, TokenType type, Location location) {
        return new Token(lexeme, type, location);
    }

    public static Token of(Location location) {
        return new Token("", TokenType.IDENTIFIER, location);
    }

    public TokenType type() {
        return type;
    }

    public Location location() {
        return location;
    }

    public List<Token> expanded() {
        return expanded;
    }

    public String lexeme() {
        return lexeme;
    }

    @Override
    public String toString() {
        return String.format("%s: %s", location, lexeme);
    }
}
