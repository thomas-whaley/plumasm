package org.whaleythom.plum.asm;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws IOException {
        Compiler compiler = new Compiler();
        String fileName = "test.pasm";
        List<Token> lexedTokens = compiler.lex(fileName);
        Set<String> labels = compiler.getLabels(lexedTokens);
        List<Token> tokens = compiler.updateLabels(lexedTokens, labels);

        List<CompilerToken> compilerTokens = compiler.compile(tokens, labels, false);
        List<CompilerToken> labelLinkedTokens = compiler.jumpLabelReferences(compilerTokens);
        System.out.println(labelLinkedTokens.stream().map(t -> "(byte)" + t.value() + ", // " + t.instruction()).collect(Collectors.joining(",\n")));





//        Errorable<Lexer> lexerErrorable = Lexer.of("test.pasm");
//        if (lexerErrorable.hasError()) {
//            System.err.println(lexerErrorable.getError());
//            System.exit(1);
//        }
//        Lexer lexer = lexerErrorable.getValue();
////        lexer.debug();
//        Errorable<ASTNode> root = Parser.parse(lexer);
//        if (root.hasError()) {
//            System.err.println(root.getError());
//            System.exit(1);
//        }
////        System.out.println(root.getValue().visit(new DebugVisitor()));
//
//        Errorable<List<CompilerToken>> compiled = Compiler.compile(root.getValue(), false);
//        if (compiled.hasError()) {
//            System.err.println(compiled.getError());
//            System.exit(1);
//        }
//        System.out.println(compiled.getValue().stream().map(CompilerToken::instruction).collect(Collectors.joining("\n")));
    }
}