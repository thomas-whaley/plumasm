package org.whaleythom.plum.asm;

public record CompilerToken(Token token, String instruction, byte value, boolean isArg, boolean isLabel, boolean isReference) {
    public static CompilerToken of(Token token, String instruction, byte value) {
        return new CompilerToken(token, instruction, value, false, false, false);
    }

    public static CompilerToken arg(Token token, byte value) {
        return new CompilerToken(token, String.format("0x%x", value), value, true, false, false);
    }

    public static CompilerToken label(Token token, String label) {
        return new CompilerToken(token, label, (byte) 0, false, true, false);
    }

    public static CompilerToken reference(Token token, String label) {
        return new CompilerToken(token, label, (byte) 0, false, false, true);
    }
}
