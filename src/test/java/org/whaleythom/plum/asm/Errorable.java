package org.whaleythom.plum.asm;

public class Errorable<T> {
    private final T value;
    private final Error error;

    private Errorable(T value, Error error) {
        this.value = value;
        this.error = error;
    }

    public static <T> Errorable<T> of(T value) {
        return new Errorable<>(value, null);
    }

    public static <T> Errorable<T> of(Error error) {
        return new Errorable<>(null, error);
    }

    public boolean hasError() {
        return error != null;
    }

    public T getValue() {
        assert value != null;
        assert error == null;
        return value;
    }

    public Error getError() {
        assert hasError();
        return error;
    }
}
