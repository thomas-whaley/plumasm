package org.whaleythom.plum.asm;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Error {
    private record ErrorToken(Optional<Token> token, String message, String inlineMessage) {
        @Override
        public String toString() {
            if (token().isEmpty()) {
                return message + (inlineMessage.isEmpty() ? "" : (": " + inlineMessage));
            }
            return token().get().location() + ": " + message() + "\n" +
                    token().get().location().line().replace("\t", "    ") + "\n" +
                    " ".repeat(token().get().location().col() - 1) +
                    "^".repeat(token().get().lexeme().length()) +
                    " " + inlineMessage();
        }
    }

    private final List<ErrorToken> tokens = new ArrayList<>();

    private Error(List<ErrorToken> tokens) {
        this.tokens.addAll(tokens);
    }

    public static Error of(Token token, String message) {
        return new Error(List.of(new ErrorToken(Optional.of(token), message, "")));
    }
    public static Error of(Token token, String message, String inlineMessage) {
        return new Error(List.of(new ErrorToken(Optional.of(token), message, inlineMessage)));
    }

    public static Error of(String message) {
        return new Error(List.of(new ErrorToken(Optional.empty(), message, "")));
    }

    public Error addError(Error error) {
        this.tokens.addAll(error.tokens);
        return this;
    }

    public Error addMessage(String message) {
        this.tokens.add(new ErrorToken(Optional.empty(), message, ""));
        return this;
    }

    public String toString() {
        return tokens.stream().map(ErrorToken::toString).collect(Collectors.joining("\n"));
    }

    public void show() {
        System.err.println(this);
        System.exit(1);
    }
}
