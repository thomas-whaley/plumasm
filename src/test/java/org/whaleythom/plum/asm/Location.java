package org.whaleythom.plum.asm;

public record Location(String file, String line, int lineNum, int col) {

    @Override
    public String toString() {
        return String.format("%s:%d:%d", file, lineNum, col);
    }
}
